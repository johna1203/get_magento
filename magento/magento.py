#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'johna'

import datetime
import random
import string
import uuid
import time
from git import Repo
import os
import shutil
import xml.etree.ElementTree as ET
import subprocess
import MySQLdb
import logging
from logging import FileHandler

GIT_PATH = "https://johna1203@bitbucket.org/johna1203/my-magento.git"
TMP_MAGENTO_PATH = os.environ.get("TMP_MAGENTO_PATH", "/data/http")
MAGENTO_PATH = os.environ.get("MAGENTO_PATH", "/data/http")
MAGENTO_MAINTENANCE_PATH = "%s/%s" % (MAGENTO_PATH, "maintenance.flag")
GET_MAGENTO_LOG_PATH = os.environ.get("LOG_PATH", "/data/logs/get_magento.log")

class GetMagento():
    def __init__(self):  # コンストラクタ
        self.name = ""
        logging.basicConfig(level=logging.DEBUG)
        self.logger = logging.getLogger(__name__)
        file_handler = FileHandler(GET_MAGENTO_LOG_PATH, 'a+')
        file_handler.level = logging.INFO
        self.logger.addHandler(file_handler)

    def prepare(self):
        indexHtmlFile = "%s/%s" % (MAGENTO_PATH, "index.html");
        with open(MAGENTO_MAINTENANCE_PATH, 'w') as fp:
            fp.write("<h1>esta preparando o magento....</h1>");

        # criar o banco primeiro...
        if self.setup_db() == False:
            self.write_log("nao deu  para criar o banco....")

        #clonar o Magento...
        self.download(True)

        out_xml = self.make_xml()
        shutil.copy(out_xml, "%s/%s" % (MAGENTO_PATH, "app/etc/local.xml"))
        os.remove(out_xml)


        # # preparar o xml
        # localXmlPath = self.make_xml()

        # create magento user
        self.create_admin_user()

        #clean files
        self.cleanFiles()

    def get_current_dir(self):
        return os.path.dirname(os.path.abspath(__file__))

    def randomword(self, length):
        return ''.join(random.choice(string.lowercase + string.uppercase + string.digits) for i in range(length))

    def write_log(self, message, t="info"):
        if t == "info":
            self.logger.info(message)
        elif t == "error":
            self.logger.error(message)
        else:
            self.logger.debug(message)

        with open(MAGENTO_MAINTENANCE_PATH, 'w') as fp:
            fp.write(message)

    def download(self, flg=False):
        if flg and os.path.exists(TMP_MAGENTO_PATH):
            self.write_log("remove %s", TMP_MAGENTO_PATH)
            shutil.rmtree(TMP_MAGENTO_PATH)

        try:
            if os.path.exists(TMP_MAGENTO_PATH) and len(os.listdir(TMP_MAGENTO_PATH)) == 0:
                os.removedirs(TMP_MAGENTO_PATH)

            Repo.clone_from(GIT_PATH, TMP_MAGENTO_PATH)

            with open(MAGENTO_MAINTENANCE_PATH, 'a'):
                os.utime(MAGENTO_MAINTENANCE_PATH, None)

        except Exception, e:
            self.write_log("clone %s" % e, "error")

    def cleanFiles(self):
        if (os.path.isfile(MAGENTO_MAINTENANCE_PATH)):
            os.remove(MAGENTO_MAINTENANCE_PATH)

        indexHtmlFile = "%s/%s" % (MAGENTO_PATH, "index.html");
        if os.path.isfile(indexHtmlFile):
            os.remove(indexHtmlFile)

    def make_xml(self):
        unique_filename = uuid.uuid4()
        localXml = "%s/%s" % (self.get_current_dir(), "template/local.xml")
        outputLocalXml = "/%s/%s.xml" % ('tmp', unique_filename)
        tree = ET.parse(localXml)
        root = tree.getroot()

        now = datetime.datetime.now()
        element = root.find("global//install/date")
        element.text = now.strftime("%a, %d %b %Y %H:%M:%S +0900")

        element = root.find("global//crypt/key")
        element.text = self.randomword(50)

        element = root.find("global//resources/db/table_prefix")
        element.text = ""

        element = root.find("global//resources/default_setup/connection/host")
        element.text = str(os.environ.get("MARIADB_PORT_3306_TCP_ADDR", "localhost"))

        element = root.find("global//resources/default_setup/connection/username")
        element.text = str(os.environ.get("MARIADB_ENV_MARIADB_USER", "docker"))

        element = root.find("global//resources/default_setup/connection/password")
        element.text = str(os.environ.get("MARIADB_ENV_MARIADB_PASS", "yourmariadbpassword"))

        element = root.find("global//resources/default_setup/connection/dbname")
        element.text = str(os.environ.get("DB_DBNAME", "magento"))

        element = root.find("global//resources/default_setup/connection/initStatements")
        element.text = "SET NAMES utf8"

        element = root.find("global//resources/default_setup/connection/model")
        element.text = "mysql4"

        element = root.find("global//resources/default_setup/connection/type")
        element.text = "pdo_mysql"

        element = root.find("admin//routers/adminhtml/args/frontName")
        element.text = str(os.environ.get("FRONTNAME", "admin"))

        tree.write(outputLocalXml, encoding='utf-8', xml_declaration=True)

        return outputLocalXml

    def setup_db(self):
        """
        Path "/data/http/app/etc" must be writable.
        Path "/data/http/media" must be writable.
        Path "/data/http/media/customer" must be writable.
        Path "/data/http/media/dhl" must be writable.
        Path "/data/http/media/dhl/logo.jpg" must be writable.
        Path "/data/http/media/downloadable" must be writable.
        Path "/data/http/media/xmlconnect" must be writable.
        Path "/data/http/media/xmlconnect/custom" must be writable.
        Path "/data/http/media/xmlconnect/custom/ok.gif" must be writable.
        Path "/data/http/media/xmlconnect/original" must be writable.
        Path "/data/http/media/xmlconnect/original/ok.gif" must be writable.
        Path "/data/http/media/xmlconnect/system" must be writable.
        Path "/data/http/media/xmlconnect/system/ok.gif" must be writable.
        """
        try_count = 5
        success_flg = False
        while try_count > 0:
            if self.create_magento_db():
                success_flg = True
                break;

            self.write_log("creating databese")
            success_flg = False
            try_count -= 1
            time.sleep(10)

        if success_flg:
            return True

        self.write_log("cant setup_db", "error")
        return False

    def create_admin_user(self):
        working_directory = "%s/%s" % (MAGENTO_PATH, "shell")
        username = "--username %s" % os.environ.get("USER_NAME")
        password = "--password %s" % os.environ.get("PASSWORD")
        firstname = "--firstname %s" % os.environ.get("FIRSTNAME")
        lastname = "--lastname %s" % os.environ.get("LASTNAME")
        email = "--email %s" % os.environ.get("EMAIL")
        domain = os.environ.get("VIRTUAL_HOST", "localhost")

        command = "%s %s %s %s %s %s" % (
            "/usr/bin/php -f create_admin_user.php --", username, password, firstname, lastname, email)
        p = subprocess.call(command, cwd=working_directory, shell=True)
        self.write_log("finish create user %s" % p)
        if p != 'OK':
            pass

        command = "magentoadmin config:set web/seo/use_rewrites 1"
        p = subprocess.call(command, cwd=MAGENTO_PATH, shell=True)
        self.write_log("config:set web/seo/use_rewrites 1: %s" % p)

        command = "magentoadmin config:set web/unsecure/base_url \"http://%s/\"" % domain
        p = subprocess.call(command, cwd=MAGENTO_PATH, shell=True)
        self.write_log("config:set web/unsecure/base_url: %s" % p)

        command = "magentoadmin config:set web/secure/base_url \"http://%s/\"" % domain
        p = subprocess.call(command, cwd=MAGENTO_PATH, shell=True)
        self.write_log("config:set web/secure/base_url: %s" % p)

        template_code = os.environ.get("TEMPLATE_CODE")
        if template_code:
            command = "/usr/bin/php -f template.php -- --template %s" % template_code
            p = subprocess.call(command, cwd=working_directory, shell=True)
            self.write_log("set template : %s" % template_code)

        command = "magentoadmin cache:enable"
        p = subprocess.call(command, cwd=MAGENTO_PATH, shell=True)
        self.write_log("cache:enable: %s" % p)

        # reindexall
        command = "/usr/bin/php -f indexer.php -- reindexall"
        p = subprocess.call(command, cwd=working_directory, shell=True)
        self.write_log("finish index: %s" % p)


    def create_magento_db(self):
        db_host = os.environ.get("MARIADB_PORT_3306_TCP_ADDR", "localhost")
        db_user = os.environ.get("MARIADB_ENV_MARIADB_USER", "docker")
        db_password = os.environ.get("MARIADB_ENV_MARIADB_PASS")
        db_name = os.environ.get("DB_DBNAME", "magento")
        sql_file = "%s/template/dump.sql" % self.get_current_dir()
        try:
            db1 = MySQLdb.connect(host=db_host, user=db_user, passwd=db_password)
            cursor = db1.cursor()
            sql = 'CREATE DATABASE %s' % MySQLdb.escape_string(db_name)

            cursor.execute(sql)
            self.write_log("mysql restore")
            os.popen("mysql -h %s -u %s -p%s %s < %s" % (db_host, db_user, db_password, db_name, sql_file))
        except Exception, e:
            self.write_log("error : %s" % e, "error")
            return False

        return True

    def check_file(self):
        localXml = "%s/%s" % (self.get_current_dir(), "template/local.xml")
        print localXml
        print os.path.exists(localXml)


if __name__ == '__main__':
    os.environ["MARIADB_PORT_3306_TCP_ADDR"] = "127.0.0.1"
    os.environ["MARIADB_ENV_MARIADB_USER"] = "docker"
    os.environ["MARIADB_ENV_MARIADB_PASS"] = "yourmariadbpassword"
    os.environ["DB_DBNAME"] = "magento"

    os.environ["USER_NAME"] = "johna12"
    os.environ["PASSWORD"] = "testtest"
    os.environ["FIRSTNAME"] = "Johnathan"
    os.environ["LASTNAME"] = "Froeming"
    os.environ["EMAIL"] = "johna1203@gmail.com"
    os.environ["FRONTNAME"] = "admin"

    getmagento = GetMagento()

    # db1 = MySQLdb.connect(host=os.environ.get("MARIADB_PORT_3306_TCP_ADDR"), user=os.environ.get("MARIADB_ENV_MARIADB_USER"),
    #                       passwd=os.environ.get("MARIADB_ENV_MARIADB_PASS"))
    # cursor = db1.cursor()
    # sql = 'DROP DATABASE %s' % MySQLdb.escape_string(os.environ.get("DB_DBNAME"))
    # try:
    #     cursor.execute(sql)
    # except Exception, e:
    #     print "error : %s" % e
    # finally:
    #     db1.close()

    getmagento.prepare()

    logging.info("download ok")
