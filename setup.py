#!/usr/bin/env python
from __future__ import print_function
from setuptools import setup, find_packages
from pip.req import parse_requirements

install_reqs = parse_requirements('requirements.txt', session=False)
reqs = [str(ir.req) for ir in install_reqs]

setup(name='get-magento',
      version='1.0.9',
      description='get magento and setup',
      author='Johnathan David',
      author_email='johnathan@kodokuman.com',
      url='http://www.geekylab.net',
      # package_dir={'magento': 'src'},
      packages=find_packages('.'),
      scripts=['bin/get_magento', 'bin/magentoadmin'],
      py_modules=['magento.magento'],
      package_data={'magento': ['template/*']},
      install_requires=reqs,
      zip_safe=False,
      )
